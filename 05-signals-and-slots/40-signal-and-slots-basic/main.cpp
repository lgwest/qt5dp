#include <QCoreApplication>

/*
  What: Signals and slots
  Description: The moc that connects objects
  Why: Inter-object communication

  Exmaple:
  Simple example of signals and slots, look at MOC.
  Using QShared pointer.

  Look at the MOC files
  look at the CPP files for each

*/

#include <QObject>
#include "producer.h"
#include "consumer.h"

int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);

    Producer producer;
    Consumer consumer;

    //Connect the objects
    QObject::connect(&producer,&Producer::readyProduct, &consumer,&Consumer::readyProduct);
    QObject::connect(&a,&QCoreApplication::aboutToQuit, &producer,&Producer::stop);

    producer.start();

    return a.exec();
}
