#include "producer.h"

Producer::Producer(QObject *parent) : QObject(parent)
{
    m_timer.setInterval(3000);

    //Connect the two objects
    connect(&m_timer,&QTimer::timeout,this,&Producer::produced);
}

void Producer::start()
{
    qInfo() << this << "Producer starting";
    m_timer.start();
}

void Producer::stop()
{
    m_timer.stop();
    qInfo() << this << "Producer stopped";
}

void Producer::produced()
{
    qInfo() << this << "Producing item";
    QSharedPointer<MyClass> ptr(new MyClass());
    qInfo() << this << "Item produced, sending to test";
    emit readyProduct(ptr);
    qInfo() << this << "after emit";
    // Here is MyClass destructor called
}
