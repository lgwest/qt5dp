#include "consumer.h"
#include <QTime>

Consumer::Consumer(QObject *parent) : QObject(parent)
{

}

void Consumer::readyProduct(QSharedPointer<MyClass> ptr)
{
    qInfo() << this << "Consumer is working";
    ptr->test("Testing the item: " + QTime::currentTime().toString());
    qInfo() << this << "Consumer has finished";
}
