# Lecture notes

## Memory patterns
### 32 The Stack
- QObject subclasses are not copyable https://cleanqt.io/blog/why-qobject-subclasses-are-not-copyable

## signals and slots
### basic

~~~
Producer(0x30cd51fd18) Producer starting
Producer(0x30cd51fd18) Producing item
MyClass(0x1bf8013ce10) Constructed
Producer(0x30cd51fd18) Item produced, sending to test
Consumer(0x30cd51fd68) Consumer is working
MyClass(0x1bf8013ce10) "Testing the item: 12:15:57"
Consumer(0x30cd51fd68) Consumer has finished
Producer(0x30cd51fd18) after emit
MyClass(0x1bf8013ce10) Destroyed
Producer(0x30cd51fd18) Producing item
MyClass(0x1bf8013d2c0) Constructed
Producer(0x30cd51fd18) Item produced, sending to test
Consumer(0x30cd51fd68) Consumer is working
MyClass(0x1bf8013d2c0) "Testing the item: 12:16:00"
Consumer(0x30cd51fd68) Consumer has finished
Producer(0x30cd51fd18) after emit
MyClass(0x1bf8013d2c0) Destroyed
~~~


~~~cpp
// main.cpp
#include <QCoreApplication>

/*
  What: Signals and slots
  Description: The moc that connects objects
  Why: Inter-object communication

  Exmaple:
  Simple example of signals and slots, using QShared pointer.

  Look at the MOC files
  look at the CPP files for each

*/


#include <QObject>
#include <QDebug>
#include "producer.h"
#include "consumer.h"

int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);

    Producer producer;
    Consumer consumer;

    //Connect the objects
    QObject::connect(&producer,&Producer::readyProduct, &consumer,&Consumer::readyProduct);
    QObject::connect(&a,&QCoreApplication::aboutToQuit, &producer,&Producer::stop);

    producer.start();

    return a.exec();
}
~~~

~~~cpp
// producer.h
#ifndef PRODUCER_H
#define PRODUCER_H

#include <QObject>
#include <QTimer>
#include <QDebug>
#include <QSharedPointer>
#include "myclass.h"


class Producer : public QObject
{
    Q_OBJECT
public:
    explicit Producer(QObject *parent = nullptr);

public slots:
    void start();
    void stop();

private slots:
    void produced();

signals:
    void readyProduct(QSharedPointer<MyClass> ptr);

private:
    QTimer m_timer;

};

#endif // PRODUCER_H

~~~

~~~cpp
// producer.cpp
#include "producer.h"

Producer::Producer(QObject *parent) : QObject(parent)
{
    m_timer.setInterval(3000);

    //Connect the two objects
    connect(&m_timer,&QTimer::timeout,this,&Producer::produced);
}

void Producer::start()
{
    qInfo() << this << "Producer starting";
    m_timer.start();
}

void Producer::stop()
{
    m_timer.stop();
    qInfo() << this << "Producer stopped";
}

void Producer::produced()
{
    qInfo() << this << "Producing item";
    QSharedPointer<MyClass> ptr(new MyClass());
    qInfo() << this << "Item produced";
    emit readyProduct(ptr);
    qInfo() << this << "after emit";
    // Here is MyClass destructor called}
~~~

~~~cpp
// consumer.h
#ifndef CONSUMER_H
#define CONSUMER_H

#include <QObject>
#include <QDebug>
#include <QSharedPointer>
#include <QTime>
#include "myclass.h"

class Consumer : public QObject
{
    Q_OBJECT
public:
    explicit Consumer(QObject *parent = nullptr);

signals:

public slots:
    void readyProduct(QSharedPointer<MyClass> ptr);
};

#endif // CONSUMER_H
~~~

~~~cpp
// consumer.cpp
#include "consumer.h"
#include <QTimer>

Consumer::Consumer(QObject *parent) : QObject(parent)
{
}

void Consumer::readyProduct(QSharedPointer<MyClass> ptr)
{
    qInfo() << this << "Consumer is working";
    ptr->test("Testing the product: " + QTime::currentTime().toString());
    qInfo() << this << "Consumer has finished";
}
~~~

~~~cpp
// myclass.cpp
#ifndef MYCLASS_H
#define MYCLASS_H

#include <QObject>
#include <QDebug>

class MyClass : public QObject
{
    Q_OBJECT
public:
    explicit MyClass(QObject *parent = nullptr);
    ~MyClass();
    void test(QString value);
signals:

};

#endif // MYCLASS_H
~~~

~~~cpp
// myclass.cpp
#include "myclass.h"

MyClass::MyClass(QObject *parent) : QObject(parent)
{
    qInfo() << this << "Constructed";
}

MyClass::~MyClass()
{
    qInfo() << this << "Destroyed";
}

void MyClass::test(QString value)
{
    qInfo() << this << value;
}
~~~
