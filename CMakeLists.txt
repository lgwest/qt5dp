cmake_minimum_required(VERSION 3.14)

project(qt5dp)

macro(add_exe name)
	# name of exe is equal to  prefix + name of first source file without extension
	# get prefix, that is, the name of current directory
	get_filename_component(prefix ${CMAKE_CURRENT_LIST_DIR} NAME)
	string(REPLACE " " "_" prefix ${prefix})
	# remove extension of first source file
	get_filename_component(name_noext ${name} NAME_WE)
	# put together name of executable, name is equal to the first, and sometimes the only, element in ${ARGV}
	add_executable(${prefix}-${name_noext} ${ARGV})
	target_link_libraries(${prefix}-${name_noext} Qt5::Core Qt5::Gui Qt5::Widgets Qt5::Network Qt5::Quick)
endmacro()

set(CMAKE_AUTOMOC ON)
set(CMAKE_AUTORCC ON)
set(CMAKE_AUTOUIC ON)

if(CMAKE_VERSION VERSION_LESS "3.7.0")
    set(CMAKE_INCLUDE_CURRENT_DIR ON)
endif()

find_package(Qt5 COMPONENTS Core Widgets Network Quick REQUIRED)

add_subdirectory(01-welcome)
#add_subdirectory(02-class-patterns)
#add_subdirectory(03-exception-patterns)
#add_subdirectory(04-memory-patterns)
#add_subdirectory(05-signals-and-slots)
#add_subdirectory(06-thread-patterns)
#add_subdirectory(07-template-patterns)
#add_subdirectory(08-creational-patterns)
#add_subdirectory(09-structural-patterns)
#add_subdirectory(10-behavioral-patterns)
add_subdirectory(11-qt-state-machine-framework)
