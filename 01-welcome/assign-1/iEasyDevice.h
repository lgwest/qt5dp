#pragma once

#include <QIODevice>
#include <QByteArray>

class iEasyDevice 
{
public:
	virtual void toDevice(QByteArray &data) = 0;
	virtual QByteArray fromDevice() = 0;
};
